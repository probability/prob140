.. prob140 documentation master file, created by
   sphinx-quickstart on Sun Nov  6 14:34:51 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================
Welcome to prob140's documentation!
===================================

The ``prob140`` documentation has been moved to `prob140.org/prob140 <http://prob140.org/prob140/>`_.
Please follow the link if you are not automatically redirected.
